<?php

namespace Mgnl\VendorCleaner;

use Composer\Script\Event;

final class Cleaner
{

    /**
     * @param Event $event
     *
     * @return void
     */
    public static function postProdUpdate(Event $event): void
    {
        if ($event->isDevMode()) {
            return;
        }

        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');

        $ds = DIRECTORY_SEPARATOR;

        foreach (Config::getRules($vendorDir) as $item) {
            self::clearVendor($item, true);
        }
    }

    /**
     * @param string $dir
     * @param bool   $withIt
     *
     * @return bool
     */
    private static function clearVendor(string $dir, bool $withIt = false): bool
    {
        if (is_dir($dir)) {

            $items = array_diff(scandir($dir), ['..', '.']);

            foreach ($items as $item) {

                $object = $dir.'/'.$item;

                if ('dir' === filetype($object)) {

                    if (!self::clearVendor($dir.'/'.$item)) {
                        return false;
                    }

                    $directory = $dir.'/'.$item;

                    if (!rmdir($dir.'/'.$item)) {

                        echo '[NO REMOVE]'.$directory."\n";

                        return false;
                    } else {
                        echo '[REMOVED]'.$directory."\n";
                    }
                } else {
                    if (!unlink($object)) {

                        echo '[NO REMOVE]'.$object."\n";

                        return false;
                    } else {
                        echo '[REMOVED]'.$object."\n";
                    }
                }
            }

            if ($withIt) {
                return rmdir($dir);
            }

            return true;

        } elseif (is_file($dir)) {

            if (unlink($dir)) {
                echo '[REMOVED]'.$dir."\n";
            }
        }

        return false;
    }
}
