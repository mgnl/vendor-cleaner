<?php

namespace Mgnl\VendorCleaner;

final class Config
{

    /**
     * Directories
     *
     * @const array
     */
    const D = [
        'docs',
        'examples',
        'public',
        'tests',
    ];

    /**
     * Files
     *
     * @const array
     */
    const F = [// Files
        '.doctrine-project.json',
        '.editorconfig',
        '.gitattributes',
        '.gitignore',
        '.php_cs',
        '.php_cs.dist',
        '.scrutinizer.yml',
        '.travis.yml',
        'CHANGELOG.md',
        'CONTRIBUTING.md',
        'Dockerfile',
        'FAQ.md',
        'LICENSE',
        'LICENSE.txt',
        'README.md',
        'UPGRADE.md',
        'UPGRADING.md',
        'VERSION',
        'composer.json',
        'composer.lock',
        'phpbench.json.dist',
        'phpdoc.xml',
        'phpmd.xml',
        'phpstan.neon',
        'phpunit.xml',
        'phpunit.xml.dist',
        'psalm.xml.dist',
        'puli.json',
    ];

    /**
     * @var string
     */
    private static $vendor;

    /**
     * @var array
     */
    private static $items = [];

    /**
     * @param string $vendorDir
     *
     * @return array
     */
    public static function getRules(string $vendorDir): array
    {
        self::$vendor = $vendorDir;

        $rules = [
            'clue/stream-filter'                => [self::D, self::F],
            'chillerlan/php-qrcode'             => [self::D, self::F],
            'chillerlan/php-settings-container' => [self::D, self::F],
            'doctrine/annotations'              => [self::D, self::F],
            'doctrine/cache'                    => [self::D, self::F],
            'doctrine/collections'              => [self::D, self::F],
            'doctrine/event-manager'            => [self::D, self::F],
            'doctrine/lexer'                    => [self::D, self::F],
            'doctrine/persistence'              => [self::D, self::F],
            'doctrine/reflection'               => [self::D, self::F],
            'firebase/php-jwt'                  => [self::F],
            'guzzlehttp/guzzle'                 => [self::F],
            'guzzlehttp/promises'               => [self::F, 'Makefile'],
            'guzzlehttp/psr7'                   => [self::F],
            'http-interop/http-factory-guzzle'  => [self::F],
            'jean85/pretty-package-versions'    => [self::F],
            'mgnl/magnolia'                     => [self::F, self::D],
            'mgnl/vendor-cleaner'               => [self::F, self::D],
            'mk-j/php_xlsxwriter'               => [self::D, self::F, 'testbench', 'example-cli.php', 'example.php'],
            'monolog/monolog'                   => [self::D, self::F],
            'php-http/client-common'            => [self::D, self::F],
            'php-http/discovery'                => [self::D, self::F],
            'php-http/guzzle6-adapter'          => [self::D, self::F],
            'php-http/httplug'                  => [self::D, self::F],
            'php-http/message'                  => [self::D, self::F],
            'php-http/message-factory'          => [self::D, self::F],
            'php-http/promise'                  => [self::D, self::F],
            'phpmailer/phpmailer'               => ['language', 'COMMITMENT', self::F, 'SECURITY.md'],
            'predis/predis'                     => [self::F, 'package.ini'],
            'psr/cache'                         => [self::F],
            'psr/container'                     => [self::F],
            'psr/event-dispatcher'              => [self::F],
            'psr/http-client'                   => [self::F],
            'psr/http-factory'                  => [self::F],
            'psr/http-message'                  => [self::F],
            'psr/log'                           => [self::F],
            'psr/simple-cache'                  => [self::F],
            'ralouphie/getallheaders'           => [self::D, self::F],
            'sensio/framework-extra-bundle'     => [self::F, 'Tests'],
            'sentry/sentry'                     => [self::F, 'AUTHORS', 'UPGRADE-2.0.md'],
            'sentry/sentry-symfony'             => [self::F, 'AUTHORS', 'UPGRADE-3.0.md'],


            'symfony/browser-kit'                => [self::F, 'Tests'],
            'symfony/cache'                      => [self::F, 'Tests'],
            'symfony/cache-contracts'            => [self::F],
            'symfony/config'                     => [self::F, 'Tests'],
            'symfony/console'                    => [self::F, 'Tests'],
            'symfony/css-selector'               => [self::F],
            'symfony/contracts'                  => [self::F, 'Tests'],
            'symfony/debug'                      => [self::F, 'Tests'],
            'symfony/dependency-injection'       => [self::F, 'Tests'],
            'symfony/dom-crawler'                => [self::F, 'Tests'],
            'symfony/dotenv'                     => [self::F, 'Tests'],
            'symfony/error-handler'              => [self::F, 'Tests'],
            'symfony/event-dispatcher'           => [self::F, 'Tests'],
            'symfony/event-dispatcher-contracts' => [self::F, 'Tests'],
            'symfony/filesystem'                 => [self::F, 'Tests'],
            'symfony/finder'                     => [self::F, 'Tests'],
            'symfony/flex'                       => [self::F, 'Tests'],
            'symfony/framework-bundle'           => [self::F, 'Tests'],
            'symfony/http-foundation'            => [self::F, 'Tests'],
            'symfony/http-kernel'                => [self::F, 'Tests'],
            'symfony/mime'                       => [self::F, 'Tests'],
            'symfony/monolog-bridge'             => [self::F, 'Tests'],
            'symfony/monolog-bundle'             => [self::F, 'Tests'],
            'symfony/options-resolver'           => [self::F],
            'symfony/phpunit-bridge'             => [self::F, 'Tests'],
            'symfony/polyfill-intl-idn'          => [self::F, 'Tests'],
            'symfony/polyfill-mbstring'          => [self::F, 'Tests'],
            'symfony/polyfill-php73'             => [self::F, 'Tests'],
            'symfony/polyfill-uuid'              => [self::F],
            'symfony/routing'                    => [self::F, 'Tests'],
            'symfony/security-core'              => [self::F],
            'symfony/service-contracts'          => [self::F, 'Tests'],
            'symfony/test-pack'                  => [self::F],
            'symfony/templating'                 => [self::F, 'Tests'],
            'symfony/var-dumper'                 => [self::F, 'Tests'],
            'symfony/var-exporter'               => [self::F, 'Tests'],
            'symfony/yaml'                       => [self::F, 'Tests'],
        ];

        foreach ($rules as $basePath => $rule) {

            if (empty($rule)) {
                continue;
            }

            self::retrievePaths($basePath, $rule);
        }

        return self::$items;
    }

    /**
     * @param string $basePath
     * @param array  $rule
     *
     * @return void
     */
    private static function retrievePaths(string $basePath, array $rule): void
    {
        foreach ($rule as $items) {
            self::retirieveItemPath($basePath, (array)$items);
        }
    }

    /**
     * @param string $basePath
     * @param array  $items
     *
     * @return void
     */
    private static function retirieveItemPath(string $basePath, array $items): void
    {
        foreach ($items as $item) {
            self::addItem($basePath, $item);
        }
    }

    /**
     * @param string $basePath
     * @param string $item
     *
     * @return void
     */
    private static function addItem(string $basePath, string $item): void
    {
        $path = self::$vendor . '/' . $basePath . '/' . $item;

        if (file_exists($path)) {
            self::$items[] = $path;
        }
    }
}
