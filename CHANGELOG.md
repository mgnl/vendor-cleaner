
### 1.1.0 (2020-03-02)

    * The files list has been updated (JWT, PSR, Symfony 5)

### 1.0.0 (2019-05-01)

    * Initial release